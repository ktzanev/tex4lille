# [tex4lille](https://gitlab.com/ktzanev/tex4lille) / [web page](https://ktzanev.gitlab.io/tex4lille/)

Les documents pour l'enseignement de LaTeX qui accompagne les modules « Explorations Mathématiques » et « Devoirs faits » du L2 Mathématiques à l'Université de Lille.

## Contenu

Dans [ce dépôt](https://gitlab.com/ktzanev/tex4lille) vous pouvez trouver les sources LaTeX et les PDFs (compilés avec XeLaTeX ou [tectonic](https://tectonic-typesetting.github.io/en-US/)) des documents suivants :

- Cours 1 [[pdf](TeX4_2023_Cours1.pdf)] [[tex](TeX4_2023_Cours1.tex)] [[exemple](exemples/ExempleCours1.tex)], _18 et 25  janvier 2023_
- Cours 2 [[pdf](TeX4_2023_Cours2.pdf)] [[tex](TeX4_2023_Cours2.tex)] [[exemple](exemples/ExempleCours2.tex)], _01 et 08 février 2023_
- Cours 3 [[pdf](TeX4_2023_Cours3.pdf)] [[tex](TeX4_2023_Cours3.tex)] [[exemple](exemples/ExempleCours3.tex)], _08 et 15 mars 2023_

J'ai fait aussi une présentation « plus » contenant des choses que je ne vais probablement pas avoir le temps de discuter pendant les 3 séances de ce module.

- Cours + [[pdf](TeX4_2023_Cours_plus.pdf)] [[tex](TeX4_2023_Cours_plus.tex)], _non fait en 2023_

Pour compiler ces fichiers vous avez besoin de la classe [tex4_beamer.cls](tex4_beamer.cls), ainsi que des images
 [overleaf.pdf](overleaf.pdf) et [download.pdf](download.pdf).

[Ce dépôt](https://gitlab.com/ktzanev/tex4lille) contient aussi des modèles de démarrage :

- [ModeleXeLaTeX.tex](exemples/ModeleXeLaTeX.tex) : modèle de rapport pour `XeLaTeX` et `LuaLaTeX` ;
- [ModeleTikZ.tex](exemples/ModeleTikZ.tex) : modèle pour image en Ti*k*Z ;

ainsi que quelques autres modèles non abordés dans ce cours :

- [ModelePDFLaTeX.tex](exemples/ModelePDFLaTeX.tex) : modèle de rapport pour `PDFLaTeX` ;
- [ModeleToutLaTeX.tex](exemples/ModeleToutLaTeX.tex) : modèle de rapport *(pour tout LaTeX)* ;
- [ModelePSTricks.tex](exemples/ModelePSTricks.tex) : modèle pour image en PSTricks *(à compiler avec `XeLaTeX`)*.

Ces modèles ainsi que les fichiers d'exemples sont disponibles dans le dossier [exemples](https://gitlab.com/ktzanev/tex4lille/-/tree/master/exemples).

## Utilisation

Vous pouvez obtenir [ce dépôt](https://gitlab.com/ktzanev/tex4lille) de trois façons faciles :

- vous pouvez télécharger les fichiers un à un ;
- en téléchargeant le [zip](https://gitlab.com/ktzanev/tex4lille/-/archive/master/tex4lille-master.zip) qui contient la dernière version de tous les fichiers ;
- en clonant le dépôt entier, l'historique y compris, en utilisant la commande `git` suivante
  ```shell
  git clone https://gitlab.com/ktzanev/tex4lille.git .
  ```

## Évaluation

Les critères d'évaluation de vos travaux sont disponibles sur [la page dédiée](criteres_evaluation_LaTeX.md).

## Historique

### 2022

La version de 2022 est accessible dans [le même dépôt, avec le tag v2022](https://gitlab.com/ktzanev/tex4lille/-/tree/v2022). Vous pouvez trouver les sources LaTeX et les PDFs (compilés avec XeLaTeX ou [tectonic](https://tectonic-typesetting.github.io/en-US/)) des documents suivants :

- Cours 1 [[pdf](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2022/TeX4_2022_Cours1.pdf)] [[tex](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2022/TeX4_2022_Cours1.tex)], _26 janvier et 02 février 2022_
- Cours 2 [[pdf](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2022/TeX4_2022_Cours2.pdf)] [[tex](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2022/TeX4_2022_Cours2.tex)], _23 février et 02 mars 2022_
- Cours 3 [[pdf](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2022/TeX4_2022_Cours3.pdf)] [[tex](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2022/TeX4_2022_Cours3.tex)], _23 et 30 mars 2022_
- Cours + [[pdf](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2022/TeX4_2022_Cours_plus.pdf)] [[tex](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2022/TeX4_2022_Cours_plus.tex)], _non fait en 2022_

### 2021

La version de 2021 est accessible dans [le même dépôt, avec le tag v2021](https://gitlab.com/ktzanev/tex4lille/-/tree/v2021). Vous pouvez trouver les sources LaTeX et les PDFs (compilés avec XeLaTeX ou [tectonic](https://tectonic-typesetting.github.io/en-US/)) des documents suivants :

- Cours 1 [[pdf](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2021/TeX4_2021_Cours1.pdf)] [[tex](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2021/TeX4_2021_Cours1.tex)], _22 et 29 janvier 2021_
- Cours 2 [[pdf](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2021/TeX4_2021_Cours2.pdf)] [[tex](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2021/TeX4_2021_Cours2.tex)], _05 et 12 février 2021_
- Cours 3 [[pdf](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2021/TeX4_2021_Cours3.pdf)] [[tex](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2021/TeX4_2021_Cours3.tex)], _12 et 19 mars 2021_
- Cours + [[pdf](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2021/TeX4_2021_Cours_plus.pdf)] [[tex](https://glcdn.githack.com/ktzanev/tex4lille/-/raw/v2021/TeX4_2021_Cours_plus.tex)], _non fait en 2021_

### Avant

Ce cours est basé sur des cours précédents :

- [TeX4](https://ktzanev.github.io/tex4lille1/) de 2017
- [TeX412](https://ktzanev.github.io/tex412lille1/) de 2015

Il partage aussi beaucoup de contenus avec :

- [m1opm-tex](https://ktzanev.gitlab.io/m1opm-tex/)

## Licence

MIT
