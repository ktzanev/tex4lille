# Critère pour l'évaluation des documents LaTeX

## Les points qui seront évalués positivement en cas d'une utilisation appropriée

- Préambule :
  - correctement commenté;
  - minimaliste :
    - on ne garde pas des bibliothèques non nécessaires ;
    - ne pas garder des macros non utilisées ;
    - on n'utilise pas des bibliothèques spécifiques à `pdflatex` dans un projet `xelatex`, par exemple.
  - bien organisé :
    - les bibliothèques et leurs options doivent être organisé logiquement ;
    - il est souhaitable que vos travaux puissent être compilés aussi bien avec `xelatex` qu'avec `pdflatex`.
- Code correctement formaté :
  - mise en retrait dans les environnements ;
  - on ne met pas en retrait des (sous-)sections entiers ;
  - passages à la ligne qui favorise la lecture du code.
- La présence, l'utilisation adéquate et la variété des éléments suivants seront appréciées :
  - table des matières correctement utilisée ;
  - théorèmes/propositions/... correctement déclarés et utilisés ;
  - environnements de math (`align`, `gather`, `multiline`...) ;
  - images et figures ;
  - TikZ (PSTricks, metapost...) ;
  - listes (numérotés, non numéroté, paramétrisation...) ;
  - utilisation de la bibliothèque `hyperref` ;
  - références (`\label`, `\ref`, `\eqref`, `\notag`...) ;
  - bibliographie et citations ;

## Les points qui seront évalués négativement

- Bricolage « à la main » en lieu et place d'une commande LaTeX :
  - par exemple faire une liste avec des « - » au début de la ligne ;
  - autre exemple : écrire les références « en dur » au lieu d'utiliser `label/ref` ;
- Utilisation de `\\`, `\newline`, `\smallskip`..., sauf dans les rares cas justifiés.
- Utilisation des guillemets "américain" à la place des guillemets « français ».
- Mauvaise typographie (ne pas oublier points et virgules en fin des formules quand c'est nécessaire).
- Erreurs et avertissements de compilation.
- « Overfulls » (débordement en dehors des marges).

## Aspect général

L'aspect final du pdf sera aussi évalué _(de façon probablement un peu subjective)_.
